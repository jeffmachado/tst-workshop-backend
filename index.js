var express = require('express');
var app = express();
var port = process.env.PORT || 8080;

app.get('/', function (req, res) {
  res.send('GET Running!');
});

app.listen(port, function () {
  console.log('API running on port '+port+'.');
});